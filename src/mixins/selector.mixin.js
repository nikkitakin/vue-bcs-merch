export default {
  props: {
    label: {
      requred: true,
      type: String,
    },
    value: {
      required: true,
      type: Object,
    },
    isActive: {
      required: true,
      type: Boolean,
    },
    tooltip: {
      required: false,
      type: String,
      default: '',
    },
  },

  methods: {
    onClick() {
      this.$emit('click', this.value);
    },
  },
};
