import _ from 'lodash';
import { mapState } from 'vuex';

export default {
  computed: {
    ...mapState({
      items: (state) => state.merch.items,
    }),

    totlaItems() {
      return this.$store.getters.getMerchItemsCount();
    },

    totalDesiredItems() {
      return this.$store.getters.getMerchDesiredItemsCount();
    },
  },

  methods: {
    getItemIndexById(itemId) {
      return _.findIndex(this.$store.state.merch.items, (item) => item.id === itemId);
    },
  },
};
