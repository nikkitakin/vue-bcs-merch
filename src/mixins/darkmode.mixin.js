export default {
  computed: {
    darkMode() {
      return this.$route.meta.darkMode;
    },
  },
};
