import Vue from 'vue';
import VueRouter from 'vue-router';

// merch
import Product from '@/views/merch/Product/Product.vue';
import Showcase from '@/views/merch/Showcase/Showcase.vue';
import Merch from '@/views/merch/Merch/Merch.vue';
import End from '@/views/merch/End/End.vue';
import Export from '@/views/merch/Export/Export.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Merch,
    props: true,
  },
  {
    name: 'merch-showcase',
    path: '/merch/showcase',
    component: Showcase,
    props: true,
    meta: { darkMode: true },
  },
  {
    name: 'product',
    path: '/merch/items/:id',
    component: Product,
    props: true,
    meta: { darkMode: false },
  },
  {
    path: '/merch/end',
    name: 'merch-end',
    component: End,
  },
  {
    path: '/merch/export',
    name: 'export',
    component: Export,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    return { x: 0, y: 0 };
  },
});

export default router;
