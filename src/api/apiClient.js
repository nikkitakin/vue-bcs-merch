import axios from 'axios';
import _ from 'lodash';
import config from '@/config';

const methods = ['get', 'post', 'put', 'patch', 'delete'];

class Client {
  static instance;

  constructor(apiRoot, apiOptions = {}) {
    if (this.instance) {
      return this.instance;
    }

    this.api = axios.create({
      baseURL: apiRoot,
      timeout: 30000,
    });

    this.defaultConfig = {
      headers: {
        Accept: 'application/json',
        'Accept-Language': 'en-US',
        'Content-Type': 'application/json',
      },
    };

    methods.forEach((method) => {
      this[method] = (path, data, options) => {
        const resultConfig = _.merge({}, this.defaultConfig, options, apiOptions, {
        });
        const resultApi = method === 'get' || method === 'delete' || method === 'head' || method === 'options'
          ? this.api[method](path, resultConfig)
          : this.api[method](path, data, resultConfig);
        return resultApi;
      };
    });
    this.instance = this;
  }
}

// Api client for backendless api
export default new Client(config.backendApi.root);
