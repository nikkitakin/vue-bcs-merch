import Vue from 'vue';
import PerfectScrollbar from 'vue2-perfect-scrollbar';
import VuePageTransition from 'vue-page-transition';
import vmodal from 'vue-js-modal';
import router from './router';
import store from './store';
import App from './App.vue';

// Stylesheet
import './assets/scss/main.scss';
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css';
import 'swiper/swiper-bundle.css';

Vue.use(VuePageTransition);
Vue.use(PerfectScrollbar);
Vue.use(vmodal);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
