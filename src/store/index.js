import Vue from 'vue';
import Vuex from 'vuex';
// import createPersistedState from 'vuex-persistedstate';

// Modules
import merch from './modules/merch';
import showcase from './modules/showcase';
import users from './modules/users';
import notifications from './modules/notifications';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    merch,
    showcase,
    users,
    notifications,
  },
});
