import Vue from 'vue';

export default {
  state: () => ({
    maxAllowedItems: 2,
    items: [
      // T-SHIRT
      {
        id: 't-shirt',
        title: 'Футболка',
        description: 'Футболка с круглым вырезом из натурального хлопка. Свободный или классический крой на твой выбор.',

        modelOptions: [
          {
            id: 0,
            label: 'Oversize',
            description: 'Модель свободного кроя',
          },
          {
            id: 1,
            label: 'Basic',
            description: 'Модель классического кроя',
          },
        ],
        pickedModel: 0,

        sizeOptions: [
          {
            label: 'xs',
          },
          {
            label: 's',
          },
          {
            label: 'm',
          },
          {
            label: 'l',
          },
          {
            label: 'xl',
          },
          {
            label: '2xl',
          },
        ],
        pickedSize: 'm',

        colorOptions: [
          {
            id: 0,
            label: 'Черный',
            hex: '#000000',
          },
          {
            id: 1,
            label: 'Off-white',
            hex: '#ffffff',
          },
        ],
        pickedColor: 0,

        printOptions: [
          {
            id: 0,
            label: '1',
            description: 'Логотип на спине',
            bindToKey: 'back',
          },
          {
            id: 1,
            label: '2',
            description: 'Без принта на спине',
            bindToKey: 'back',
          },
        ],
        pickedPrint: 0,

        galleryKeys: ['front', 'back'],

        desire: null,
      },
      // HOODIE
      {
        id: 'hoodie',
        title: 'Худи',
        description: 'Толстовка с капюшоном из плотного хлопка<br>в угольно-черном или оф-вайт варианте.',

        sizeOptions: [
          {
            label: 'xs',
          },
          {
            label: 's',
          },
          {
            label: 'm',
          },
          {
            label: 'l',
          },
          {
            label: 'xl',
          },
          {
            label: '2xl',
          },
        ],
        pickedSize: 'm',

        colorOptions: [
          {
            id: 0,
            label: 'Черный',
            hex: '#000000',
          },
          {
            id: 1,
            label: 'Off-white',
            hex: '#ffffff',
          },
        ],
        pickedColor: 0,

        printOptions: [
          {
            id: 0,
            label: '1',
            description: 'Принт на рукавах',
          },
        ],
        pickedPrint: 0,

        galleryKeys: ['front', 'layout'],

        desire: null,
      },
      // BAGPACK
      {
        id: 'backpack',
        title: 'Рюкзак',
        description: 'Рюкзак из материала Tyvek. Легкий и прочный, устойчив к дождю и снегу. Вес пустого изделия — 400 грамм.',
        sizeOptions: [
          {
            label: 'One size',
          },
        ],
        pickedSize: 'One size',
        colorOptions: [
          {
            id: 0,
            label: 'Черный',
            hex: '#000000',
          },
          {
            id: 1,
            label: 'Белый',
            hex: '#ffffff',
          },
        ],
        pickedColor: 0,
        printOptions: [
          {
            id: 0,
            label: '1',
            description: 'Логотип в углу',
          },
          {
            id: 1,
            label: '2',
            description: 'Диагональ',
          },
        ],
        pickedPrint: 0,
        galleryKeys: ['front', 'back', 'inside'],
        desire: null,
      },
      // SHOPPER
      {
        id: 'shopper',
        title: 'Сумка-шоппер',
        description: 'Шопер из плотого хлопка. Можно использовать вместо продуктового пакета или как повседневную сумку.',
        sizeOptions: [
          {
            label: 'One size',
          },
        ],
        pickedSize: 'One size',
        colorOptions: [
          {
            id: 0,
            label: 'Черный',
            hex: '#000000',
          },
          {
            id: 1,
            label: 'Off-white',
            hex: '#ffffff',
          },
        ],
        pickedColor: 0,
        printOptions: [
          {
            id: 0,
            label: '1',
            description: 'Логотип в углу',
          },
          {
            id: 1,
            label: '2',
            description: 'Большой логотип',
          },
        ],
        pickedPrint: 0,
        galleryKeys: ['front', 'front-alt'],
        desire: null,
      },

      /* {
        id: 'cup',
        title: 'Кружка-термос',
        description: `Кружка с двойными стальными стенками. Сохраняет тепло до 8 часов.
          В крышку термоса встроен фильтр — удобно использовать для заварки листового чая.
        `,
        colorOptions: [
          {
            id: 0,
            label: 'Черный матовый',
            hex: '#000000',
          },
        ],
        sizeOptions: [
          {
            label: 'Ø6,7 х 23 см',
          },
        ],
        pickedSize: 'Ø6,7 х 23 см',
        pickedColor: 0,
        galleryKeys: ['front', 'irl'],
        desire: null,
        distortion: false,
      },

      {
        id: 'pin',
        title: 'Пин',
        description: `Значок Centra на основании из латуни.
          Внутрення часть пина покрыта мягкой эмалью белого или черного цвета.
        `,
        colorOptions: [
          {
            id: 0,
            label: 'Черная эмаль',
            hex: '#000000',
          },
          {
            id: 1,
            label: 'Белая эмаль',
            hex: '#ffffff',
          },
        ],
        sizeOptions: [
          {
            label: '3 x 3 см',
          },
        ],
        pickedSize: '3 x 3 см',
        pickedColor: 0,
        galleryKeys: ['front', 'irl'],
        desire: null,
        distortion: false,
      }, */
    ],
  }),

  getters: {
    getMerchItemById: (state) => (id) => state.items.find((item) => item.id === id),

    getMerchModelById: (state) => (id, modelId) => {
      const product = state.items.find((item) => item.id === id).modelOptions;
      return (product.find((model) => model.id === modelId));
    },

    getMerchColorById: (state) => (id, colorId) => {
      const product = state.items.find((item) => item.id === id).colorOptions;
      return (product.find((color) => color.id === colorId));
    },

    getMerchPrintById: (state) => (id, printId) => {
      const product = state.items.find((item) => item.id === id).printOptions;
      return (product.find((print) => print.id === printId));
    },

    getMerchDesiredItemsCount: (state) => () => {
      const items = state.items.filter((item) => (item.desire === true));
      return items.length;
    },

    getMerchItemsCount: (state) => () => state.items.length,

    getMerchOrderPayload: (state) => () => {
      const items = state.items.filter(
        (item) => (item.desire !== null) && (item.desire !== false),
      );
      const payload = {
        items: [],
      };
      items.forEach((item) => {
        const itemPayload = {
          id: item.id,
          title: item.title,
        };
        if (item.modelOptions) {
          itemPayload.model = item.modelOptions.find(
            (option) => option.id === item.pickedModel,
          ).label;
        }
        if (item.sizeOptions) {
          itemPayload.size = item.pickedSize;
        }
        if (item.colorOptions) {
          itemPayload.color = item.colorOptions.find(
            (option) => option.id === item.pickedColor,
          ).label;
        }
        if (item.printOptions) {
          itemPayload.print = item.printOptions.find(
            (option) => option.id === item.pickedPrint,
          ).description;
        }
        payload.items.push(itemPayload);
      });

      return payload;
    },

  },

  actions: {
    pickSizeForItem({ commit }, payload) {
      const item = this.getters.getMerchItemById(payload.itemId);
      if (item) {
        commit('setSize', {
          item,
          value: payload.size,
        });
      }
    },

    pickPrintForItem({ commit }, payload) {
      const item = this.getters.getMerchItemById(payload.itemId);
      commit('setPrint', {
        item,
        value: payload.print,
      });
    },

    pickColorForItem({ commit }, payload) {
      const item = this.getters.getMerchItemById(payload.itemId);
      commit('setColor', {
        item,
        value: payload.colorId,
      });
    },

    pickModelForItem({ commit }, payload) {
      const item = this.getters.getMerchItemById(payload.itemId);
      commit('setModel', {
        item,
        value: payload.modelId,
      });
    },

    setMerchItemDesire({ state, commit }, payload) {
      console.log(payload);
      if (payload.item.id === 'shopper') {
        commit('setMerchItemDesire', payload);
      } else {
        state.items.forEach((item) => {
          switch (item.id) {
            case payload.item.id:
              commit('setMerchItemDesire', {
                item: payload.item,
                desire: payload.desire,
              });
              break;
            case 'shopper': break;
            default:
              commit('setMerchItemDesire', {
                item,
                desire: false,
              });
              break;
          }
        });
      }
    },
  },

  mutations: {
    setSize(state, payload) {
      Vue.set(payload.item, 'pickedSize', payload.value);
    },

    setMerchItemDesire(state, payload) {
      Vue.set(payload.item, 'desire', payload.desire);
    },

    setPrint(state, payload) {
      Vue.set(payload.item, 'pickedPrint', payload.value);
    },

    setColor(state, payload) {
      Vue.set(payload.item, 'pickedColor', payload.value);
    },

    setModel(state, payload) {
      Vue.set(payload.item, 'pickedModel', payload.value);
    },
  },
};
