export default {
  state: () => ({
    showcaseWarningSeen: false,
  }),

  mutations: {
    setShowcaseWarningSeen(state, status) {
      state.showcaseWarningSeen = status;
    },
  },
};
