export default {
  state: () => ({
    scrollPosition: {
      windowWidth: false,
      x: 0,
    },
  }),

  getters: {
    getShowcaseScrollPosition: (state) => () => {
      if (
        (!state.scrollPosition.windowWidth)
        || (state.scrollPosition.windowWidth === window.innerWidth)
      ) {
        return state.scrollPosition.x;
      }
      return state.scrollPosition.x * (state.scrollPosition.windowWidth / window.innerWidth);
    },
  },

  mutations: {
    setScrollPosition(state, position) {
      state.scrollPosition = position;
    },
  },
};
